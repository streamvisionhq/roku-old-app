Sub playVideo(videoId As string)
	m.screen = CreateObject("roScreen", true)
	m.player = CreateObject("roVideoPlayer")
	m.port = CreateObject("roMessagePort")
	inputFromDevice = CreateObject("roInput")
    inputFromDevice.SetMessagePort(m.port)
	m.player.setMessagePort(m.port)
	m.screen.setMessagePort(m.port)
	m.screen.setAlphaEnable(true)
	m.fontRegistry = CreateObject("roFontRegistry")
	m.fontRegistry.Register("file://pkg:/assets/fonts/Roboto/Roboto-Medium.ttf")
	m.loadingPercentFont = m.fontRegistry.getFont("Roboto Medium" ,20, true, false)
	m.loadingTextFont = m.fontRegistry.getFont("Roboto Medium" ,40, true, false)
	m.loadingFont = m.fontRegistry.getFont("Roboto Medium" ,15, true, false)
	m.ChannelLogo = m.fontRegistry.getFont("Roboto Medium" ,25, true, false)
	m.ErrorMsg = m.fontRegistry.getFont("Roboto Medium" ,20, true, false)
	m.ErrorMsg2 = m.fontRegistry.getFont("Roboto Medium" ,30, true, false)
	m.loadingBitmap = CreateObject("roBitmap", {width:1280, height:720, AlphaEnable: true})
	m.loadingBitmap.DrawRect(260, 650, 765, 8, &hCCCCCCFF)
	m.loadingMsgX=(m.deviceInfoData.w-(len(m.loadingMsg)*20))\2
	m.screen.DrawText(m.loadingMsg , m.loadingMsgX, 300, &hFFFFFFFF, m.loadingTextFont) 
	m.loadingBitmap.DrawText("0%" , 950, 620, &hFFFFFFFF, m.loadingPercentFont)
	m.screen.DrawObject(0, 0, m.loadingBitmap)
	m.screen.SwapBuffers()
	m.epgBitmapX = 0
	m.epgBitmapY = 0
	m.onlyOnce=true
	m.epgChnlImageX=35
	m.epgChnlImageY=8
	m.epgBitmapWidth = 400
	m.epgBitmapHeight = 720
	m.chnlDivLineX=0
	m.chnlDivLineY=78
	m.chnlDivLineHeight=2
	m.chnlDivLineWidth=400
	m.drawRectFlag=true 
	m.chnlSelctBoxX=0
	m.chnlSelctBoxY=318
	m.chnlSelctBoxHeight=82
	m.chnlSelctBoxWidth=400
	m.chnlSelctBoxInnerX=2
	m.chnlSelctBoxInnerY=320
	m.chnlSelctBoxInnerHeight=78
	m.chnlSelctBoxInnerWidth=396
	m.channelLogoRepo=[]
	m.epgFlag=true
	m.epgUpDownFlag=false
	m.chnlCount=0
	m.playButton=CreateObject("roBitmap","pkg:/images/play.png")
	m.pauseButton=CreateObject("roBitmap","pkg:/images/pause.png")
	m.fastForwardButton=CreateObject("roBitmap","pkg:/images/fastforward.png")
	m.fastRewindButton=CreateObject("roBitmap","pkg:/images/fastrewind.png")
	' m.selectedChannel=4
	m.chnlCount2=m.totalChannels.data.Count()-1
	m.drawProgressBar = videoScreenDrawProgressBar
	m.secondsFromTime = videoScreenSecondsFromTime
	m.TimeFromSeconds = videoScreenTimeFromSeconds
	m.drawBufferProgressBar = drawVideoBufferProgressBar
	m.elapsedSeconds = 0
	m.paused = false
	m.metaDatax = {title: "", description: "", duration: "00:00:00", airdate: ""}
	m.timeCanvas = CreateObject("roBitmap", {width:1280, height:720, AlphaEnable: true})
	m.drawSettings = {
		showInfo:false,
		elapsedTime: 0
	}
	m.buttonCursor = 1
	m.showTimer = 0
	m.showTimerMax = 10
	m.dateTime = CreateObject("roDateTime")
	m.dateTime.toLocalTime()
	m.dateTime.Mark()
	m.scrubbing = false
	m.scrubRate = 0
	m.backbuttonFlag = false
	m.getMetaData = getVideoMetaData
	m.showVideo = showVideoPlayer
	m.draw = drawScreen
	m.runEventLoop = runVideoPlayerEventLoop
	m.resume = resumeVideo
	m.pause = pauseVideo
	m.videoPlaybackPosition = 0
	m.bufferProgress = 0
	m.allowOK=false
	m.lastPlayedVideoID= "-1" 'videoId

	if videoId <> "" then
		playVideoUrl=[]
		playVideoUrlSuper=[]
		setVideoUrl={}
		videoUrl={}
		videoUrl.url=videoId
		setVideoUrl.Stream=videoUrl
		setVideoUrl.StreamFormat="hls"
		setVideoUrl.SwitchingStrategy="full-adaptation"
		playVideoUrl.push(setVideoUrl)
		playVideoUrlSuper.push(playVideoUrl)
		m.showVideo(playVideoUrlSuper[0])
	end if
	m.selectedVideoURL=""
	
End Sub


Function getVideoMetaData(videoId as String) As Object

	init  = initApi()
	videoDetailsData = init.getVideoDetails(videoId)
	videoData = init.getVideo(videoId)

	if videoData <> invalid AND videoDetailsData <> invalid then  
		metadata = {}
		metadata.title = videoDetailsData.name
		metadata.duration = videoDetailsData.duration
		metadata.sbsMinRampBitrate = 64
		metadata.streamFormat = "hls"
		metadata.switchingStrategy = "full-adaptation"
		metadata.categories = [ videoDetailsData.genre.name ]
		metadata.isHD = videoDetailsData.hd
		metadata.UserStarRating = videoDetailsData.rating.toInt()
		metadata.stream = {
			url:videoData.hls_url
		}
		metadata.SubtitleConfig = {
			TrackName:videoData.video_preview_url
		}
		if (CreateObject("roDeviceInfo").GetDisplaySize().h <= 720) then
		   metadata.maxBandwidth = 4200
		endif
		xfer = CreateApiURLTransferObject("https:" + videoDetailsData.overlay_url)
		xfer.gettoFile("tmp:/loadingBackground.jpg")
		m.loadingBackground = CreateObject("roBitmap", "pkg:/images/load.png")
		m.screen.SwapBuffers()
		return metadata
	else 
		return invalid
	end if 

End Function


Sub showVideoPlayer(videoId As Object)
	m.player.SetContentList(videoId)
	m.player.SetPositionNotificationPeriod(1)
	m.player.SetDestinationRect({x:0, y:0, w:0, h:0})
	m.player.Play()
	m.draw()
	m.runEventLoop(videoId[0].stream.url)
End Sub


Sub drawScreen()
	m.screen.Clear(&h00000000)
	m.screen.SwapBuffers()
End Sub


Sub runVideoPlayerEventLoop(videoId as String)
	while true
		message = wait(0, m.port)
		if m.clock.TotalMilliseconds() > m.next_call then
		getCurrentShow()
		m.next_call=m.clock.TotalMilliseconds() + m.timeInterval
		print "Timer Called"
		end if

		if type(message) = "roUniversalControlEvent" then
			key = message.getInt()
			if key = 0 then 'BACK 
				print "################ Video Closed ################"
				m.exit="NO"
				exit while
	    	else if key = 6 then ' OK
	    		if m.allowOK=true then
	    			clearEPG()
	    			m.selectedVideoURL=getStreamUrl(m.totalChannels.data[m.selectedChannel].svChannelId.toStr())
	    			m.loadingMsg="Loading "+m.totalChannels.data[m.selectedChannel].description+" ..."
	    			if m.selectedVideoURL <> ""
	    				exit while
	    			end if
	    			if m.exit = "NO" then
	    				exit while
	    			end if
				end if
			else if key = 13 then 'play/pause
					if m.bufferProgress=100 then
					clearEPG()
	    		m.showTimer = 0
    			if m.paused then
    				m.paused = false
    				m.player.Resume()
    				m.timeCanvas.DrawObject(145,615,m.pauseButton)
    				m.screen.DrawObject(0, 0, m.timeCanvas)
    				m.screen.SwapBuffers()
    				sleep(800)
    				m.drawSettings.showInfo = false
					m.screen.Clear(&h00000000)
					m.screen.SwapBuffers()
    			else
    				m.paused = true
    				m.player.Pause()
						m.drawSettings.showInfo = true
						m.screen.Clear(&h00000000)
						m.timeCanvas.DrawObject(145,615,m.playButton)
						m.screen.DrawObject(0, 0, m.timeCanvas)
						m.screen.SwapBuffers()
    			end if
    			end if
	    	else if key = 9 then 

	    	else if key = 8 then

	    	else if key = 10 then 
	    		m.screen.Clear(&h00000000)
	    		showEPG()
	    	else if key = 2 then
	    		if m.epgUpDownFlag=true then
	    			m.screen.Clear(&h00000000)
		    		m.selectedChannel=m.selectedChannel-1
		    		if(m.selectedChannel<0) then
	    				m.selectedChannel=m.chnlCount2
	    			end if
			    	displayChalsOnEpg()
	    		end if
	    	else if key = 3 then
	    		if m.epgUpDownFlag=true then
	    			m.screen.Clear(&h00000000)
	    			m.selectedChannel=m.selectedChannel+1
	    			if m.selectedChannel>m.chnlCount2 then
	    				m.selectedChannel=0
	    			end if
		    		displayChalsOnEpg()
	    		end if
			end if
		end if

		if type(message) = "roVideoPlayerEvent" then
			if message.isPlaybackPosition()
				m.elapsedSeconds = message.GetIndex()
		        if m.drawSettings.showInfo then
		        	m.showTimer = m.showTimer + 1
		        	if m.showTimer > m.showTimerMax then
		        		m.drawSettings.showInfo = false
		        		m.screen.Clear(&h00000000)
						m.screen.SwapBuffers()
						m.showTimer = 0
					else 
						m.screen.Clear(&h00000000)
						m.screen.DrawObject(0, 0, m.timeCanvas)
						m.screen.SwapBuffers()
					end if
				end if

			else if message.isPaused() 
				print "################ Video Paused ################"
			else if message.isResumed()
			    print "################ Video Resumed ################"
			else if message.isRequestFailed() then
			    print "################ Playback Failed ################" + message.GetMessage()
			     
				if message.GetMessage() <> "" then
					clearEPG()
					m.screen.drawRect(410, 340, 450, 50, &h000000A0)
				    m.screen.DrawText("Playback Failure", 500, 350, &hFFFFFFFF, m.ErrorMsg2)
				    m.screen.SwapBuffers()
				    sleep(2000)
				    clearEPG()
				    if m.initialLoad = false then
				    	m.selectedVideoURL=m.lastPlayedVideoID
				    	print "m.selectedVideoURL"; m.selectedVideoURL
				    else
						print "first stream Error"
					end if
					exit while
				end if
				
			else if message.isFullResult() then
			    print "################ Video Ended ################"
			else if message.isPartialResult() then
			    print "################ Video Aborted ################"
			    exit while
			else if message.isStatusMessage() then
				if message.GetMessage() = "start of play"
					m.lastPlayedVideoID = videoId
					m.initialLoad=false
					print "################ Video Starts Playing ################"
					m.screen.Clear(&h00000000)
					m.screen.SwapBuffers()
					m.epgFlag=true
				else if message.GetMessage() = "startup progress"
					bufferingProgres% = message.GetIndex() / 10
					m.bufferProgress = bufferingProgres%
					print m.bufferProgress
					m.drawBufferProgressBar()
			    end if
			end if
		end if

		if type(message) = "roInputEvent" then 
            channelResponse = message.GetInfo().op
            clearEPG()
	    	TokenizedString=channelResponse.Tokenize("_")
	    	m.selectedVideoURL=getStreamUrl(TokenizedString[0])
	    	if m.selectedVideoURL <> ""
	    		print "###############" TokenizedString[0]
	    		for ax=0 to m.totalChannels.data.Count()-1 step 1
	    			if(TokenizedString[0]=m.totalChannels.data[ax].svChannelId.toStr()) then
	    				m.loadingMsg="Loading "+m.totalChannels.data[ax].description+" ..."
	    				m.selectedChannel=ax
	    			end if
	    		end for
	    	end if
	    	exit while
	    end If
	end while
	if m.exit <> "NO" then
		if m.initialLoad = false then
			if m.selectedVideoURL <> "" then
			    playVideoUrl=[]
			   	playVideoUrlSuper=[]
			    setVideoUrl={}
			    videoUrl={}
			    videoUrl.url=m.selectedVideoURL.trim()
			    setVideoUrl.Stream=videoUrl
			    setVideoUrl.StreamFormat="hls"
			    setVideoUrl.SwitchingStrategy="full-adaptation"
			    playVideoUrl.push(setVideoUrl)
			    playVideoUrlSuper.push(playVideoUrl)
			    m.player.Stop()
			    m.showVideo(playVideoUrlSuper[0])	
			end if
		else
			getFirstStream()
		end if
	end if
End Sub

Sub resumeVideo()

End Sub


Sub pauseVideo()

End Sub

function showEPG()
	if m.epgFlag=true then
		displayChalsOnEpg()
		m.epgFlag=false
		m.allowOK=true
		m.epgUpDownFlag=true
	else if m.epgFlag=false then		
		clearEPG()
		if m.bufferProgress<100
			m.drawBufferProgressBar()
		end if
	end if
end function

function displayChalsOnEpg()
	canvas=CreateObject("roBitmap", {width:400, height:720, AlphaEnable: true})
	canvas.Clear(&h404040A0)
	for z=0 to 7 step 1
		canvas.drawRect(m.chnlDivLineX,m.chnlDivLineY,m.chnlDivLineWidth,m.chnlDivLineHeight,&h000000FF)
		m.chnlDivLineY=m.chnlDivLineY+80
	end for
	'selected box black
	canvas.drawRect(m.chnlSelctBoxX,m.chnlSelctBoxY,m.chnlSelctBoxWidth,m.chnlSelctBoxHeight,&h54bf26A0)
	'selected box green
	canvas.drawRect(m.chnlSelctBoxInnerX,m.chnlSelctBoxInnerY,m.chnlSelctBoxInnerWidth,m.chnlSelctBoxInnerHeight,&h000000A0)
	counter1=0
	counter2=m.totalChannels.data.Count()
	if m.onlyOnce=true then
		m.onlyOnce=false
		for q=0 to m.totalChannels.data.Count()-1 step 1
			xfer = CreateApiURLTransferObject(m.totalChannels.data[q].channelLogo)
			tmppath = "tmp:/ThumbNailImage.jpg"
			xfer.gettoFile(tmppath)
			chnlLogo=CreateObject("roBitmap",tmppath)
			tmppath = "tmp:/ThumbNailImage.jpg"
			m.channelLogoRepo.push(chnlLogo)
		end for
	end if
	' print "------------------"; counter2
	counterXY=0
	drwTxt1X=140
	drwTxt1Y=10
	drwTxt2X=140
	drwTxt2Y=30
	drwTxt3X=140
	drwTxt3Y=45
	' print "------------------------"; m.selectedChannel
	for x=m.selectedChannel-4 to m.selectedChannel+4 step 1
		if x>m.totalChannels.data.Count()-1 then
			canvas.DrawScaledObject(m.epgChnlImageX,m.epgChnlImageY,0.9,0.9,m.channelLogoRepo[counter1])
			canvas.DrawText("Now :",drwTxt1X,drwTxt1Y,&h00BFFFFF,m.loadingPercentFont)
			drwTxt1Y=drwTxt1Y+80 
			if toStr(m.totalChannels.data[counter1].channelId) <> "" then
				for y=0 to m.nowShowResponse.data.Count()-1 step 1
					if toStr(m.totalChannels.data[counter1].channelId) = toStr(m.nowShowResponse.data[y].channelId) then
						canvas.DrawText(m.nowShowResponse.data[y].Title,drwTxt2X,drwTxt2Y,&hFFFFFFFF,m.loadingFont)
						exit for
					end if
				end for
			else
				canvas.DrawText("",drwTxt2X,drwTxt2Y,&hFFFFFFFF,m.loadingFont)
			end if
			drwTxt2Y=drwTxt2Y+80
			counter1=counter1+1
			if counter1=9 then
				m.selectedChannel=4
				m.chnlCount=0
			end if
		else if x<0
			counterXY=counterXY+1
			if counterXY=9 then
				m.selectedChannel=4
				m.chnlCount2=m.totalChannels.data.Count()-1
			end if
			canvas.DrawScaledObject(m.epgChnlImageX,m.epgChnlImageY,0.9,0.9,m.channelLogoRepo[counter2+x])
			canvas.DrawText("Now :",drwTxt1X,drwTxt1Y,&h00BFFFFF,m.loadingPercentFont)
			drwTxt1Y=drwTxt1Y+80
			' print "SSSSSSSSSSSSSSSSSS :"; counter2+x
			if toStr(m.totalChannels.data[counter2+x].channelId) <> "" then
				for y=0 to m.nowShowResponse.data.Count()-1 step 1
					if toStr(m.totalChannels.data[counter2+x].channelId) = toStr(m.nowShowResponse.data[y].channelId) then
						canvas.DrawText(m.nowShowResponse.data[y].Title,drwTxt2X,drwTxt2Y,&hFFFFFFFF,m.loadingFont)
						exit for
					end if
				end for
			else
				canvas.DrawText("",drwTxt2X,drwTxt2Y,&hFFFFFFFF,m.loadingFont)
			end if
			drwTxt2Y=drwTxt2Y+80
		else 
			canvas.DrawScaledObject(m.epgChnlImageX,m.epgChnlImageY,0.9,0.9,m.channelLogoRepo[x])
			canvas.DrawText("Now :",drwTxt1X,drwTxt1Y,&h00BFFFFF,m.loadingPercentFont)
			drwTxt1Y=drwTxt1Y+80
			if toStr(m.totalChannels.data[x].channelId) <> "" then
				for y=0 to m.nowShowResponse.data.Count()-1 step 1
					if toStr(m.totalChannels.data[x].channelId) = toStr(m.nowShowResponse.data[y].channelId) then
						canvas.DrawText(m.nowShowResponse.data[y].Title,drwTxt2X,drwTxt2Y,&hFFFFFFFF,m.loadingFont)
						exit for
					end if
				end for
			else
				canvas.DrawText("",drwTxt2X,drwTxt2Y,&hFFFFFFFF,m.loadingFont)
			end if
			drwTxt2Y=drwTxt2Y+80
		end if 
		m.epgChnlImageY=m.epgChnlImageY+80
	end for
	counter1=0
	counter2x=6
	counter2y=8
	m.screen.DrawObject(0,0,canvas)
	m.screen.SwapBuffers()
	m.chnlDivLineX=0
	m.chnlDivLineY=78
	m.epgChnlImageX=35
	m.epgChnlImageY=8
end function

function clearEPG()
	m.screen.Clear(&h00000000)
	m.screen.SwapBuffers()
	m.screen.Clear(&h00000000)
	m.epgFlag=true
	m.allowOK=false
	m.epgUpDownFlag=false
end function


sub videoScreenDrawProgressBar()
	m.timeCanvas.Clear(&h00000000)
	time = m.timeFromSeconds(m.elapsedSeconds)
    m.timeCanvas.DrawText(time, 70, 623, &h7A7A7AFF, m.loadingFont)
    'progress bar underlay
    m.timeCanvas.DrawRect(195, 632, 890, 8, &h7B7B7B90)

	if duration <> 0
    	progress = m.elapsedSeconds / duration
    	progressWidth = progress * 890
    	'draw progress
    	m.timeCanvas.DrawRect(195, 632, progressWidth, 8, &h00BFFFFF)

    	secondsRemainding = duration - m.elapsedSeconds
		time = m.timeFromSeconds(secondsRemainding)

    	'time left label
		m.timeCanvas.DrawText(time, 1100, 623, &h7A7A7AFF, m.loadingFont)
    end if

	m.screen.DrawObject(0, 0, m.timeCanvas)
end sub


function videoScreenSecondsFromTime(time as String) as Integer
    return time.toInt()
end function

function videoScreenTimeFromSeconds(seconds as Integer) as String
	
	hours = Int(seconds / 3600)
	if hours < 1
		minutes = Int(seconds / 60)
		seconds = seconds MOD 60
	else 
		minutes = Int(seconds / 60)
		minutes = minutes - (hours*60)
		seconds = seconds - (hours*3600) - (minutes*60)
	end if

	if hours < 10 then
		hourStr = "0" + hours.toStr()
	else
		hourStr = hours.toStr()
	end if

	if minutes < 10 then
		minuteStr = "0" + minutes.toStr()
	else
		minuteStr = minutes.toStr()
	end if
	if seconds < 10 then
		secondStr = "0" + seconds.toStr()
	else
		secondStr = seconds.toStr()
	end if

	return hourStr + ":" + minuteStr + ":" + secondStr
end function

sub drawVideoBufferProgressBar()

    progress = m.bufferProgress / 100
    progressWidth = progress * 765
   	m.loadingBitmap.Clear(&h00000000)
   	m.screen.SwapBuffers()
   	m.screen.Clear(&h00000000)
    m.loadingBitmap.DrawRect(260, 650, 765, 8, &hCCCCCCFF)    
    m.loadingBitmap.DrawRect(260, 650, progressWidth, 8, &h2E2EFEFF)
    m.loadingMsgX=(m.deviceInfoData.w-(len(m.loadingMsg)*20))\2
    m.screen.DrawText(m.loadingMsg, m.loadingMsgX, 300, &hFFFFFFFF, m.loadingTextFont)
	m.loadingBitmap.DrawText(m.bufferProgress.toStr() + "%" , 950, 620, &hFFFFFFFF, m.loadingPercentFont)
	m.screen.DrawObject(0, 0, m.loadingBitmap)
	m.screen.SwapBuffers()

end sub