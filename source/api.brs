'Api wrapper methods

Function initApi()
	m.apiBaseURL = "https://www.herotalkies.com/api/"
	m.buildUrl = buildUrl
	m.getRegistrationCode = getRegistrationCode
	m.getSubscriptionstatus = getSubscriptionstatus
	m.linkAccountToRoku = linkAccountToRoku
	'm.unlinkAccountOnRoku = unlinkAccountOnRoku
	m.getSiderDetails = getSiderDetails
	m.getCategoryData = getCategoryData
	m.getVideoDetails = getVideoDetailJSON
	m.searchMovie = searchMovie
	m.getPerPageData = getPerPageData
	m.getVideoDetail = getVideoDetailJSON
	m.getVideo = getVideoJSON
	'm.getMovieDetails = getMovieDetails
	m.regCode = invalid
	return m
End Function

Function buildUrl(params as string)
	url = m.apiBaseURL + params
	return url
End Function

Function getRegistrationCode()
print "inside regcode api"
	deviceID = GetSerialNumber()
	getRegistrationCodeURL = m.buildUrl("roku/get_reg_code?device_id="+ deviceID)
	print getRegistrationCodeURL
	urlObject = CreateApiURLTransferObject(getRegistrationCodeURL)
	registrationCodeResponse = urlObject.getToString()
	responseData = ParseJson(registrationCodeResponse)
	if valid(responseData["result"]["status"]) then
		status = responseData["result"]["status"]
		if status = "success" then
			regCode = responseData["result"]["regCode"]
			return regCode
		else
			print "Error"
		endif
	endif		

End Function

Function getSubscriptionstatus(userID as String)
	print "inside getSubscriptionstatus"
	getSubscriptionstatusURL = m.buildUrl("user/status?user_id="+ userID)
	print getSubscriptionstatusURL
	urlObject = CreateApiURLTransferObject(getSubscriptionstatusURL)
	getSubscriptionstatusResponse = urlObject.getToString()
	subscriptionStatus = ParseJson(getSubscriptionstatusResponse)
	return subscriptionStatus
End Function

Function linkAccountToRoku(regcode as string)
print "inside link api"
	deviceID = GetSerialNumber()
	getLinkAccountURL = m.buildUrl("user/link?reg_code="+regcode+"&device_id="+deviceID)
	print getLinkAccountURL
	urlObject = CreateApiURLTransferObject(getLinkAccountURL)
	linkAccountResponse = urlObject.getToString()
	responseData = ParseJson(linkAccountResponse)
	if valid(responseData["role"]) then
		status = responseData["role"]
		if status = "registered" then
			return status
		else
			print "Error"
		end if
	end if				
End Function

Function getSiderDetails()
	print "inside getSiderDetails"
	getSiderDetailsURL = m.buildUrl("sliders?slider_type=movie")
	print getSiderDetailsURL
	urlObject = CreateApiURLTransferObject(getSiderDetailsURL)
	getSliderDetailsResponse = urlObject.getToString()
	sliderData = ParseJson(getSliderDetailsResponse)
	return sliderData

End Function

Function getCategoryData(categoryName as String)
	
	print "inside getCategoryData"
	getCategoryDataURL = m.buildUrl("blocks/"+categoryName)
	print getCategoryDataURL
	urlObject = CreateApiURLTransferObject(getCategoryDataURL)
	getCategoryDataResponse = urlObject.getToString()
	categoryData = ParseJson(getCategoryDataResponse)
	return categoryData

End Function

Function getVideoDetailJSON(id as String)
	getVideoMetadataURL = m.buildUrl("movies/"+id)
	print getVideoMetadataURL
	urlObject = CreateApiURLTransferObject(getVideoMetadataURL)
	getVideoMetadataResponse = urlObject.getToString()
	VideoMetadataData = ParseJson(getVideoMetadataResponse)
	return VideoMetadataData
End Function

Function getVideoJSON(id as String)
	getVideoMetadataURL = m.buildUrl("movies/"+id+ "/video")
	print getVideoMetadataURL
	urlObject = CreateApiURLTransferObject(getVideoMetadataURL)
	getVideoMetadataResponse = urlObject.getToString()
	VideoMetadataData = ParseJson(getVideoMetadataResponse)
	return VideoMetadataData
End Function

Function searchMovie()
	' need Clarity from HeroTalkies guys
End Function

Function getPerPageData(pageNumber as String,requiredDataPerPage as String)
	print "inside getPerPageData"
	getPerPageDataURL = m.buildUrl("movies?page="+pageNumber+"&per_page="+requiredDataPerPage)
	print getPerPageDataURL
	urlObject = CreateApiURLTransferObject(getPerPageDataURL)
	perPageDataResponse = urlObject.getToString()
	perPageData = ParseJson(perPageDataResponse)
	return perPageData
End Function
