
Sub main()
    deviceInfo = CreateObject("roDeviceInfo")
    m.deviceInfoData = deviceInfo.GetDisplaySize()
    m.initalLoadIndex=0
	EPG_Channels_Request = CreateObject("roUrlTransfer")
    EPG_Channels_Request.SetURL("http://sv01.streamvisiontv.com/api/epg/channels")
    response = ParseJson(EPG_Channels_Request.GetToString())
    m.initialLoad=true
    m.totalChannels=""
    m.selectedChannel=0
    m.exit="YES"
    if response<>invalid then
        m.totalChannels=response
        getCurrentShow()
        m.loadingMsgX=0
        m.loadingMsg=""
        m.timeInterval=10000 * 10 * 60
        m.clock = CreateObject("roTimespan")
        m.next_call = m.clock.TotalMilliseconds() + m.timeInterval
        deviceID = getDeviceID()
        authenticationURL="http://sv01.streamvisiontv.com/api/device/"+deviceID+"/account"
        urlObject = CreateApiURLTransferObject(authenticationURL)
        authenticationResponse = urlObject.getToString()
        Activationresponse=ParseJson(authenticationResponse)
        if Activationresponse <> invalid
            if Activationresponse.success = false then
                ShowMessageDialog()
            else
                m.AcntID=Activationresponse.data.AccountID
                getFirstStream()
            end if
        else 
            print "Server Error"
            ShowErrorDialog()
        end if
    else
        print "Server Error"
        ShowErrorDialog()
    end if
End Sub

Function ShowMessageDialog() As Void 
    port = CreateObject("roMessagePort")
    dialog = CreateObject("roMessageDialog")
    dialog.EnableOverlay(true)
    dialog.SetMessagePort(port) 
    dialog.SetTitle("STREAMVISION")
    dialog.SetText("                                  ROKU DEVICE NOT LINKED")
    dialog.AddButton(1, "EXIT")
    dialog.EnableBackButton(false)
    dialog.Show()
    While True
        dlgMsg = wait(0, dialog.GetMessagePort())
        If type(dlgMsg) = "roMessageDialogEvent"
            if dlgMsg.isButtonPressed()
                if dlgMsg.GetIndex() = 1
                    exit while
                end if
            else if dlgMsg.isScreenClosed()
                exit while
            end if
        end if
    end while 
End Function


Function ShowURLnotAvailable()
    port = CreateObject("roMessagePort")
    dialog = CreateObject("roMessageDialog")
    dialog.EnableOverlay(true)
    dialog.SetMessagePort(port) 
    dialog.SetTitle("STREAMVISION")
    dialog.SetText("Stream URL not found for the selected Channel")
    dialog.AddButton(1, "OK")
    dialog.EnableBackButton(false)
    dialog.Show()
    while true
        dlgMsg = wait(1, dialog.GetMessagePort())
        If type(dlgMsg) = "roMessageDialogEvent"
            if dlgMsg.isButtonPressed()
                if dlgMsg.GetIndex() = 1
                    clearEPG()
                    exit while
                end if
            else if dlgMsg.isScreenClosed()
                clearEPG()
                exit while
            end if
        end if
    end while 
End Function


function getStreamUrl(svChannelId As String)
    streamURLObject = HttpUtils("http://sv01.streamvisiontv.com/api/customer/"+m.AcntID.toStr()+"/authorize")
    ' print "svChannelID :";svChannelId
    streamURLObject.AddParam("channelID", svChannelId)
    jsonResponse = streamURLObject.PostFromStringWithRetry("")
    jsonData=ParseJson(jsonResponse)
    ' print jsonData
    if jsonData <> invalid
        returnUrl=CreateObject("roString")
        if jsonData.success=true then
            returnUrl=jsonData.data.url
        else
            if m.screen <> invalid then
                if jsonData.message.replace("not authorized","error Message") <> jsonData.message
                    errorText=splitTextToLine(jsonData.message,m.ErrorMsg,735)
                    errorText.push("Please contact your service provider to upgrade your service.")
                    m.screen.drawRect(265, 332, 750, 25*errorText.count(), &h000000A0)
                    lineVerticalPosition=340
                    for i=0 to errorText.count()-1 step 1
                        m.screen.DrawText(errorText[i], 280, lineVerticalPosition, &hFFFFFFFF, m.ErrorMsg)
                        lineVerticalPosition=lineVerticalPosition+20
                    end for
                    m.screen.SwapBuffers()
                    sleep(5000)
                    m.epgFlag=true
                    showEPG()
                    returnUrl=""
                else
                    returnUrl="exit app"
                    displayNotAuthorizeError()
                end if
            else 
                returnUrl="exit app"
                displayNotAuthorizeError()
            end if 
        end if
    else
        print "Server Error"
        ShowErrorDialog()
    end if
    return returnUrl
end function


function getCurrentShow()
    nowShow = CreateObject("roUrlTransfer")
    nowShow.SetURL("http://sv01.streamvisiontv.com/api/epg/channels/currentShows")
    m.nowShowResponse = ParseJson(nowShow.GetToString())
    if m.nowShowResponse=invalid then
        print "Server Error"
        ShowErrorDialog()
    end if
end function

function getFirstStream()
    m.initialLoad=true
    if m.initalLoadIndex=m.totalChannels.data.count()-1 then
        m.initalLoadIndex=0
    end if
    for x=m.initalLoadIndex to m.totalChannels.data.count()-1 step 1
        m.initalLoadIndex=m.initalLoadIndex+1
        firststream=getStreamUrl(m.totalChannels.data[x].svChannelId.toStr())
        m.loadingMsg="Loading "+m.totalChannels.data[x].description+" ..."
        if firststream <> "" and firststream <> "exit app" then
            m.selectedChannel=x
            exit for
        else if firststream = "exit app"
            exit for
        end if
    end for
    if firststream <> "" and firststream <> "exit app"
        playVideo(firststream)
    else 
        displayNotAuthorizeError()
    end if
end function

Function ShowErrorDialog() As Void 
    port = CreateObject("roMessagePort")
    dialog = CreateObject("roMessageDialog")
    dialog.EnableOverlay(true)
    dialog.SetMessagePort(port) 
    dialog.SetTitle("STREAMVISION")
    dialog.SetText("                                        SERVER ERROR")
 
    dialog.EnableBackButton(true)
    dialog.Show()
    While True
        sleep(2000)
        m.exit="NO"
        exit while
    end while 
End Function

function splitTextToLine(text as String, font as Object, width = 200 as Integer, limit = 0 as Integer, token = "\\n") as Object
    if text = "" then
        return [text]
    end if

    reg = CreateObject("roRegex", token, "")
    paragraphs = reg.split(text)
    lines = []

    for i = 0 to paragraphs.count() - 1
        if limit = 0 OR lines.count() < limit Then
            words = paragraphs[i].tokenize(" ")

            if words.count() > 0 then
                line = words[0].getString()

                for j = 1 to words.Count() - 1
                    current = line + " " + words[j].getString()

                    if font.getOneLineWidth(current, width) < width and (lines.Count() < limit or limit = 0) Then
                        line = current
                    else if limit = 0 or lines.count() < limit then
                        lines.push(line)
                        line = words[j].getString()
                    else
                        exit for
                    end if
                end for

                if j <= words.count() - 1  Then
                    if font.getOneLineWidth(line + "...", width) = width Then
                        line = line.mid(0, line.len() - 4)
                    end if

                    line = line + "..."
                end if

                if lines.count() = limit and lines.count() > 0 then
                    if font.getOneLineWidth(lines[limit-1] + "...", width) = width Then
                        lines[limit-1] = lines[limit-1].mid(0, lines[limit-1].len() - 4)
                    end if
                    lines[limit-1] = lines[limit-1] + "..."
                else
                    lines.push(line)
                end if
            else
                lines.push(" ")
            end if
        else
            exit for
        end if
    end for

    if lines.count() = 0 then
        lines.push(" ")
    end if

    return lines
end function

function displayNotAuthorizeError()
    streamURLObject = HttpUtils("http://sv01.streamvisiontv.com/api/customer/"+m.AcntID.toStr()+"/authorize")
    streamURLObject.AddParam("channelID", m.totalChannels.data[(m.totalChannels.data.count()-1)].svChannelId.toStr())
    jsonResponse = streamURLObject.PostFromStringWithRetry("")
    jsonData=ParseJson(jsonResponse)
    ' print jsonData
    port = CreateObject("roMessagePort")
    dialog = CreateObject("roMessageDialog")
    dialog.EnableOverlay(true)
    dialog.SetMessagePort(port) 
    dialog.SetTitle("STREAMVISION")
    dialog.SetText(jsonData.message)
    ' dialog.SetText(Mid(jsonData.message,16,49)+" Please contact your service provider.")
    dialog.EnableBackButton(false)
    dialog.Show()
    While True
        sleep(5000)
        m.exit="NO"
        exit while
    end while
end function