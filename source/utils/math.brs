Function Ceiling(number as float) As Integer
    if number - Int(number) > 0 then
        return Int(number)+1
    else
        return Int(number)
    endif
End Function

Function RoundingHalfup(number as float) As Integer
    if number - Int(number) >= 0.5 then
        return Int(number)+1
    else
        return Int(number)
    endif
End Function