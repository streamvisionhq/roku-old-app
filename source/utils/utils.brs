
' Shows an alert with a header, a list of strings (rows) and one button. Default button is "OK"
sub showAlert(alertHeader as String, messages as Object, buttonLabel = "OK" as String)
    m.loadingIndicator.hide()
    
    port = CreateObject("roMessagePort")
    screen = CreateObject("roMessageDialog")
    
    screen.setMessagePort(port)
    screen.setTitle(alertHeader)
    screen.enableBackButton(true)
    
    for each message in messages
        screen.setText(message)
    end for 
    
    screen.addButton(1, buttonLabel)
    screen.show()

    while true
        msg = wait(1, port)
        
        if type(msg) = "roMessageDialogEvent" then        
            if msg.isScreenClosed() then
                screen.close()
                return
            end if

            if msg.isButtonPressed() then
                screen.close()
            end if
        end if
    end while
end sub

' Shows a dialog consisting of a header, a number of string (rows) and a number of buttons
' The function will return the text shown on the button the user selected. If the user clicks
' BACK then "CANCEL" will be returned
function showDialog(alertHeader as String, messages as Object, buttons as Object)
    port = CreateObject("roMessagePort")
    screen = CreateObject("roMessageDialog")
    setTheme({ screen: "dialog" })
    screen.setMessagePort(port)
    if alertHeader <> "" then
        screen.setTitle(alertHeader)
    end if
    screen.enableBackButton(true)
    
    for each message in messages
        screen.setText(message)
    end for
    
    returnMessages = []
    for i = 0 to buttons.count()-1
        screen.addButton(i, buttons[i])
        returnMessages[i] = buttons[i]
    end for
    
    screen.show()

    while true
        msg = wait(1, port)
        
        if type(msg) = "roMessageDialogEvent" then        
            if msg.isScreenClosed() then
                setTheme({ reset: true })
                return "CANCEL"
            end if

            if msg.isButtonPressed() then
                setTheme({ reset: true })
                return returnMessages[msg.getIndex()]
            end if
        end if
    end while
end function

' Shows a facade poster screen to prevent the screen stack from flickering
function showFacade()
    screen = CreateObject("roPosterScreen")
    screen.show()
    return {
        screen: screen
        close: function()
            sleep(10) ' Brief sleep to prevent screen flickering
            m.screen.close()
        end function
    }
end function


' Converts an associative array or a flat array to a JSON string
function toJSON(dataObject as Object) As String
    jsonString = "{"

    isJSONArray = false
    if isArray(dataObject) then
        isJSONArray = true
        jsonString = "["
    end if
    
    for each key in dataObject
        if isJSONArray then
            value = key
        else
            jsonString = jsonString + chr(34) + key + chr(34) + ":"
            value = dataObject[key]
        end if    
        
        if isString(value) then
            if value = "null" then
                jsonString = jsonString + value
            else
                jsonString = jsonString + chr(34) + value + chr(34)
            end if
            
        else if isInteger(value) then
            jsonString = jsonString + value.toStr()
            
        else if isFloat(value) then
            jsonString = jsonString + str(value).trim()
            
        else if isBoolean(value) then
            jsonString = jsonString + toBoolAsStr(value)
            
        else if isArray(value) then
            jsonString = jsonString + toJSON(value)
            
        else if isObject(value) then
            jsonString = jsonString + toJSON(value)
        end if
        
        jsonString = jsonString + ","
    end for
    
    if right(jsonString, 1) = "," then
        jsonString = left(jsonString, len(jsonString) - 1)
    end if
    
    if isJSONArray then
        jsonString = jsonString + "]"
    else
        jsonString = jsonString + "}"
    end if
    
    return jsonString
end function


' Writes a value to local storage
sub storePersistentValue(key as String, value as String)
    registry = createObject("roRegistrySection", "SHOWREG")
    registry.write(key, value)
    registry.flush()
end sub

' Reads a value from local storage
function readPersistentValue(key as String) as Dynamic
    registry= createObject("roRegistrySection", "SHOWREG")
    if registry.exists(key)
        return registry.read(key)
    end if
    return invalid
end function

' Deletes a value from local storage
sub deletePersistentValue(key as String)
    registry = createObject("roRegistrySection", "SHOWREG")
    if registry.exists(key)
        registry.delete(key)
        registry.flush()
    end if
end sub

' Takes a string and explodes it to an array based on a delimiter
' explode("1,2,3", ",") > ["1", "2", "3"]
function explode(str, delim) as Object
    regex = CreateObject("roRegex", delim, "")
    return regex.split(str)
end function

' Checks if an item is assigned a value
function valid(item) as Boolean
    return type(item) <> "Invalid" and type(item) <> "roInvalid" and type(item) <> "<uninitialized>"
end function

' Checks if an item is a boolean
function isBoolean(item) as Boolean
    return type(item) = "Boolean" or type(item) = "roBoolean"
end function

' Checks if an item is an integer
function isInteger(item) as Boolean
    return type(item) = "Integer" or type(item) = "roInteger" or type(item) = "roInt"
end function

' Checks if an item is a float
function isFloat(item) as Boolean
    return type(item) = "Float" or type(item) = "roFloat"
end function

' Checks if an item is a string
function isString(item) as Boolean
    return type(item) = "String" or type(item) = "roString"
end function

' Checks if an item is an array
function isArray(item) as Boolean
    return type(item) = "Array" or type(item) = "roArray"
end function

' Checks if an item is a function
function isFunction(item) as Boolean
    return type(item) = "Function" or type(item) = "roFunction"
end function

' Checks if an item is a associative array
function isObject(item) as Boolean
    return type(item) = "Object" or type(item) = "roAssociativeArray"
end function

function capitalize(word As String) as String
    word = LCase(word)
    firstLetter = Left(word,1)
    remainingLetters = mid(word, 2)
    return UCase(firstLetter) + remainingLetters  
End function

' Validates that an e-mail address is properly formatted, requiring at least the following: *@*.**
function validEmail(email as String) as Boolean
    reg = CreateObject("roRegex", ".+@.+\..+", "")
    return reg.isMatch(email)
end function

' Filters an array based on the condition provided as a function
' Example:
' list = ["apple", "banana", "pear", "pineapple"]
' result = filter(list, function(item)
'     return item.right(5) = "apple"
' end function)
' print result
' >>> ["apple", "pineapple"]
function filter(list, condition, opts = {}) as Object
    res = []
    if valid(list) and isArray(list) and valid(condition) and isFunction(condition) then
        for i = 0 to list.count() - 1
            if condition(list[i], opts) then
                res.push(list[i])
            end if
        end for
    end if
    return res
end function

' Runs through a list of items and groups them based on conditions provided as a function
' Example: 
' list = [
'     {
'         name: "John"
'         gender: "male"
'     }
'     {
'         name: "Lisa"
'         gender: "female"
'     }
'     {
'         name: "Bob"
'         gender: "male"
'     }
' ]
' grouped = group(list, function(item)
'     return item.gender ' Group by gender
' end function)
' print grouped
' >>> {
'     male: [
'          {
'              name: "John"
'              gender: "male"
'          }
'          {
'              name: "Bob"
'              gender: "male"
'          }
'     ]
'     female: [
'          {
'              name: "Lisa"
'              gender: "female"
'          }
'     ]
' }
function group(list, condition) as Object
    grouped = {}
        
    for i = 0 to list.count() - 1
        itemCondition = condition(list[i])
        if not valid(grouped[itemCondition]) then
            grouped[itemCondition] = []
        end if
        
        grouped[itemCondition].push(list[i])
    end for
    
    return grouped
end function

' Same as group, but allows the condition to return an array of
' grouping points instead of just one grouping point.
' Can be used where items in the list can belong to many groups, like:
' {
'     id: 44
'     title: "Cool Movie"
'     genres: ["Action", "Drama", "Thriller"]  
' }
function groupMany(list, condition, opts) as Object
    grouped = {}
        
    for i = 0 to list.count() - 1
        itemConditions = condition(list[i], opts)
        
        for x = 0 to itemConditions.count() -1
            if not valid(grouped[itemConditions[x]]) then
                grouped[itemConditions[x]] = []
            end if
        
            grouped[itemConditions[x]].push(list[i])
        end for
    end for
    
    return grouped
end function

' Takes an array of items and sorts them based on conditions provided as a function
function sort(array as Object, comparer as Function) as Object
    for x = 0 to array.count()-1 step 1
        for y = 0 to array.count()-2 step 1
            if comparer(array[y], array[y+1]) then
                tmp = array[y+1]
                array[y+1] = array[y]
                array[y] = tmp
            end if
        end for
    end for

    return array
end function

Sub insertionSort(A as Object, key=invalid as dynamic)

    if type(A)<>"roArray" then return

    if (key=invalid) then
        for i = 1 to A.Count()-1
            value = A[i]
            j = i-1
            while j>= 0 and A[j] > value
                A[j + 1] = A[j]
                j = j-1
            end while
            A[j+1] = value
        next

    else
        if type(key)<>"Function" then return
        for i = 1 to A.Count()-1
            ' if valid(A[i]) then 
                valuekey = key(A[i])
                value = A[i]
                j = i-1
                while j>= 0 and key(A[j]) > valuekey
                    A[j + 1] = A[j]
                    j = j-1
                end while
                A[j+1] = value
            ' endif
        next
    end if

End Sub

' Iterate over a list of items and apply a function to them
function withEach(items, func)
    for i = 0 to items.count() - 1
        func(items[i])
    end for
end function

' Iterate over a list of items and put items that match a certain condition into a new array
function collectEach(items, condition)
    collection = []
    for i = 0 to items.count() - 1
        if condition(items[i]) then
            collection.push(items[i])
        end if
    end for
    return collection
end function

' Takes a string. If the string is longer than length the string will be trimmed to that length.
' If suffix is set, the suffix will be appended to the end of the string before returning
function trim(str as String, length as Integer, suffix = "" as String) as String
    if len(str) > length then
        return str.left(length) + suffix
    else
        return str
    end if
end function

' Clone an object like array or associative array by converting it to JSON and back
' Warning! Don't use this in heavy loops. It is expensive!
function clone(obj)
    clonedObject = clonedObject
    
    if valid(obj) then
        tmpClone = toJSON(obj)
        clonedObject = ParseJson(tmpClone)
    end if
    
    return clonedObject
end function

function rdDeepCopy(v as object) as object
    v = box(v)
    vType = type(v)
    if     vType = "roArray"
        n = CreateObject(vType, v.count(), true)
        for each sv in v
            n.push(rdDeepCopy(sv))
        end for
    elseif vType = "roList"
        n = CreateObject(vType)
        for each sv in v
            n.push(rdDeepCopy(sv))
        end for
    elseif vType = "roAssociativeArray"
        n = CreateObject(vType)
        for each k in v
            n[k] = rdDeepCopy(v[k])
        end for
    elseif vType = "roByteArray"
        n = CreateObject(vType)
        n.fromHexString( v.toHexString() )
    elseif vType = "roXMLElement"
        n = CreateObject(vType)
        n.parse( v.genXML(true) )
    elseif vType = "roInt" or vType = "roFloat" or vType = "roString" or vType = "roBoolean" or vType = "roFunction" or vType = "roInvalid"
        n = v
    else
        print "skipping deep copy of component type "+vType
        n = invalid
        'n = v
    end if
    return n
end function


' Takes a string and removes all \n characters
function removeNewlines(text as String) as String
    regex = CreateObject("roRegEx", "\n", "")
    return regex.replaceAll(text, " ")
end function

' Takes a string and removes everything that is not a digit
function removeWords(text as String) as String
    regex = CreateObject("roRegEx", "[^\d+]", "")
    return regex.replaceAll(text, "")
end function

' Checks if an item is in an array
function inArray(list, item) as Boolean
    res = false
    
    if valid(list) and isArray(list) then
        for i = 0 to list.count() - 1
            if list[i] = item then
                return true
            end if
        end for
    end if
    
    return res
end function

function reverseArrayOrder(roArray as Object) as Object
    reversedArray = []
    
    for i=0 to roArray.Count()-1
        reversedArray.Push(roArray.Pop())
    end for
    
    return reversedArray
end function

' Inserts an item in an array at a given position
function insertInArray(list, item, position)
    if valid(list.count()) then
        for i = list.count()-1 to position step - 1
            list[i + 1] = list[i]
        end for
        list[position] = item
    end if
    return list
end function

' Converts a condition to a boolean and returns the string representation of that boolean
function toBoolAsStr(condition)
    if condition then
        return "true"
    else
        return "false"
    end if
end function

' Checks if app is running in HD mode or not
function isHD() as Boolean
    deviceInfo = CreateObject("roDeviceInfo")
    if deviceInfo.getDisplayMode() = "720p" then
        return true
    else
        return false
    end if
end function

function getFormat() as String
    if isHD() then
        return "hd"
    else
        return "sd"
    end if
end function

function getCoordinates(opts as Object) as Object
    ret = {
        x: 0
        y: 0
        w: 0
        h: 0
    }
    hd = isHD()
    if isInteger(opts.x) then
        if hd then
            ret.x = opts.x
        else
            ret.x = int(opts.x * 0.563)
        end if
    end if
    if isInteger(opts.y) then
        if hd then
            ret.y = opts.y
        else
            ret.y = int(opts.y * 0.667)
        end if
    end if
    if isInteger(opts.w) then
        if hd then
            ret.w = opts.w
        else
            ret.w = int(opts.w * 0.5625)
        end if
    end if
    if isInteger(opts.h) then
        if hd then
            ret.h = opts.h
        else
            ret.h = int(opts.h * 0.6667)
        end if
    end if
    return ret
end function

function getKeys() as Object
    if m.keyCodes = invalid then
        m.keyCodes = {
            BACK: 0
            UP: 2
            DOWN: 3
            LEFT: 4
            RIGHT: 5
            OK: 6
            INSTANT_REPLAY: 7
            REW: 8
            FF: 9
            INFO: 10
            PLAY: 13
        }
    end if
    return m.keyCodes
end function

function getTimeAsAMPM(dateObject as Dynamic) as String
    dateString = ""
    amPm = ""
    minutes = ""

    hour = dateObject.getHours()
    if hour >= 12 then
        amPm = "PM"
        hour = hour - 12
    else
        amPm = "AM"
    end if
    if hour = 0 then
        hour = 12
    end if

    if dateObject.getMinutes() < 10 then
        minutes = "0" + str(dateObject.getMinutes()).trim()
    else
        minutes = str(dateObject.getMinutes()).trim()
    end if

    dateString = str(hour).trim() + ":" + minutes + amPm
    return dateString
end function

' Returns the timezone as a two/three letter string
function getTimeZoneShort() as String
    tz = getConfig().TIMEZONE

    if tz = "US/Hawaii" then
        return "HT"
    else if tz = "US/Alaska" then
        return "AKT"
    else if tz = "US/Pacific" then
        return "PT"
    else if tz = "US/Mountain" or tz = "US/Arizona" then
        return "MT"
    else if tz = "US/Central" then
        return "CT"
    else if tz = "US/Eastern" then
        return "ET"
    else
        return tz
    end if
end function

function formatDateAsWeekDayDate(dateObject as Dynamic) as String
    ' asDateString("short-weekday") returns "Tue September 17, 2013". Keep the first three letters.
    weekday = ucase(left(dateObject.asDateString("short-weekday"), 3))

    month = str(dateObject.getMonth()).trim()
    date = str(dateObject.getDayOfMonth()).trim()

    return weekday + " " + month + "/" + date
end function

function formatDateAsNumeric(dateObject as Dynamic) as String

    month = str(dateObject.getMonth()).trim()
    date = str(dateObject.getDayOfMonth()).trim()
    year = right(str(dateObject.getYear()).trim(), 2)

    return  month + "." + date + "." + year
end function

function formatTime(dateObject as Dynamic) as String
    
    hours = str(dateObject.getHours()).trim()
    minutes = str(dateObject.getMinutes()).trim()
    seconds = str(dateObject.getSeconds()).trim()

    return hours + ":" + minutes + ":" + seconds
end function

function formatTimeHHMM(dateObject as Dynamic) as String

    hours_int = dateObject.getHours()
    minutes_int = dateObject.getMinutes()

    if hours_int < 10 then
        hours = "0" + str(hours_int).trim()
    else
        hours = str(hours_int).trim()
    end if

    if minutes_int < 10 then
        minutes = "0" + str(minutes_int).trim()
    else
        minutes = str(minutes_int).trim()
    end if
    return hours + ":" + minutes
end function


' Cookies!
function isCookie(name as String) as Boolean
    cookie = false
    check = instr(1, name, "Set-Cookie")
    if check <> 0 then
        cookie = true
    end if
    return cookie
end function

function storeCookies(urlEvent as Object)
    cookies = getCookiesAsObject()
    cookieAdded = false
    cookieRemoved = false
    if type(urlEvent) = "roUrlEvent" then
        header = urlEvent.GetResponseHeadersArray()
        for each item in header
            for each element in item
                ' Check if it is a cookie
                if instr(1, element, "Set-Cookie") <> 0 then
                    newCookie = makeCookie(item[element])
                    if newCookie.value = "" or newCookie.value = (chr(34)+chr(34)) or newCookie.value = "deleted" then
                        cookies.delete(newCookie.key)
                        cookieRemoved = true
                        print "Cookie deleted: " ; newCookie.key
                    else
                        cookies[newCookie.key] = newCookie.value
                        cookieAdded = true
                    end if
                    
                end if
            end for
        end for

        if cookieAdded or cookieRemoved then
            cookieJSON = toJSON(cookies)
            if valid(cookieJSON) then
                storePersistentValue("cookies", cookieJSON)
                print "Cookie stored: "
                print cookies
            else
                print "WARNING: Could not store cookies"
            end if
        end if
    end if
end function

function makeCookie(cookieString as String) as Object
    key = ""
    value = ""

    leftPos = instr(1, cookieString, "=")
    rightPos = instr(leftPos, cookieString, ";")
    ' If there's no trailing ;
    if rightPos = 0 then
        rightPos = len(cookieString)
    end if

    key = left(cookieString, leftPos - 1)
    value = mid(cookieString, leftPos + 1, rightPos - leftPos - 1)
    return {
        key: key
        value: value
    }
end function

function getCookiesAsObject() as Object
    cookies = {}
    stored = readPersistentValue("cookies")
    if isString(stored) and stored <> "" then
        parsedCookie = ParseJson(stored)
        if isObject(parsedCookie) then
            cookies = parsedCookie
        else 
            Print "WARNING: could not parse stored cookie"
        end if
    end if
    return cookies
end function

function getCookiesAsString() as String
    cookieString = ""
    cookies = getCookiesAsObject()
    
    for each cookie in cookies
        cookieString = cookieString + cookie + "=" + cookies[cookie] + ";"
    end for

    return cookieString
end function

Function getColour(r As Integer, g As Integer, b As Integer, a = 255 As Integer) As Integer

    components = [b, g, r]
    colour = a
        
    For i = 1 To 3
        colour = colour Or components[i - 1] * 256 ^ i
    Next i
    
    return colour

End Function

function secondsToMinutes(seconds) as Integer
    return int(seconds / 60)
end function

function convertJSONTimeToSec(json = "") as String
    timeProperties = [
        "now"
        "created"
        "originalAirdate"
        "start"
        "expirationDate"
    ]
    regexStr = Chr(34) + "("
    for i = 0 to timeProperties.count()-1
        regexStr = regexStr + timeProperties[i]
        if i < timeProperties.count()-1 then
            regexStr = regexStr + "|"
        end if
    end for
    regexStr = regexStr + ")" + Chr(34) + ":([0-9]{13})"
    regex = CreateObject("roRegex", regexStr, "i")

    match = regex.match(json)

    ' json = regex.ReplaceAll(json, Chr(34) + "\1" + Chr(34) + ":" + left("\2", 10))

    while match.count() >= 2
        prop = match[1]
        sec = left(match[2], len(match[2]) - 3)
        json = regex.Replace(json, Chr(34) + prop + Chr(34) + ":" + sec)
        
        ' Re-do the search
        match = regex.match(json)
    end while
    return json
end function

function stripHTML(text as String) as String
    parsedText = ""
    carriageReturn = chr(13)
    newline = chr(10)

    ' Replace <br /> with newline
    regexStr = "<br />|<br>|</br>|<br/>"
    regex = CreateObject("roRegex", regexStr, "i")
    parsedText = regex.ReplaceAll(text, newline)

    ' Remove all other tags
    regex = CreateObject("roRegex", "<[a-z]+>", "i")
    parsedText = regex.ReplaceAll(parsedText, "")

    return parsedText
end function

' Decode html characters
function decodeEscapedHTML(text as String) as String
    regex = CreateObject("roRegex", "&#[0-9]+;", "")
    splitted = regex.split(text)

    while regex.match(text).count() > 0
        matches = regex.match(text)
        part = matches[0]
        partialRegex = CreateObject("roRegex", part, "")
        asciiNoAsString = mid(part, 3, len(part)-3)
        asciiNoAsInt = asciiNoAsString.toInt()
        text = partialRegex.replaceAll(text, chr(asciiNoAsInt))
    end while

    return text
end function

' Opens a pin entry screen and asks for DOB input
function checkAge() as Integer
    storedValueStr = readPersistentValue("ageCheck")
    nowDate = CreateObject("roDatetime")
    nowDateSeconds = nowDate.asSeconds()
    if valid(storedValueStr) then
        storedValue = ParseJson(storedValueStr)
        print storedValue

        if isObject(storedValue) and valid(storedValue.date) and valid(storedValue.age) and (storedValue.date + (24 * 60 * 60)) > nowDateSeconds then
            ' User has entered a birth date within the last 24 hours
            return storedValue.age
        else
            ' User has entered a birth date for more than 24 hours ago. Clear stored date
            deletePersistentValue("ageCheck")
        end if
    end if

    reset:
    ageInYears = -1
    dobString = pinShowScreen(getCopy().DATE_OF_BIRTH.TITLE, { numberOfFields: 8 })
    print "dobString: ";dobString
    if dobString = "CANCEL" then
        return ageInYears
    else if dobString = "INVALID" then
        showAlert("", ["Please enter a valid date"])
        goto reset
    end if
    month = left(dobString, 2)
    day = mid(dobString, 3, 2)
    year = right(dobString, 4)
    print "year: ";year ; " "; type(year)
    print "month: ";month ; " "; type(month)
    print "day: ";day ; " " ; type(day)
    if year.toInt() < 1900 or year.toInt() > nowDate.getYear() then
        showAlert("", ["Please enter a valid year"])
        goto reset
    else if month.toInt() < 1 or month.toInt() > 12 then
        showAlert("", ["Please enter a valid month"])
        goto reset
    else if day.toInt() < 1 or day.toInt() > 31 then
        showAlert("", ["Please enter a valid date"])
        goto reset
    end if

    ' Create a string with the format "YYYY-MM-DD HH:MM:SS"
    dobFormatted = year + "-" + month + "-" + day + " 00:00:00"

    birthDate = CreateObject("roDateTime")
    birthDate.fromISO8601String(dobFormatted)

    if valid(birthDate) then
        ageInSeconds = nowDate.asSeconds() - birthDate.asSeconds()

        ' 1 year = 31 556 926 seconds
        ageInYears = int(ageInSeconds / 31556926)

        ' Store the entered age
        storedValue = {
            date: nowDateSeconds
            age: ageInYears
        }
        storedValueStr = toJSON(storedValue)
        storePersistentValue("ageCheck", storedValueStr)
    else
        showAlert("", ["Please enter a valid date"])
        goto reset
    end if
    return ageInYears
end function

function splitText(text as String, length = 550 as Integer)
    textArr = []
    newlineChar = getCopy().NEWLINE
    if not isHD() then
        length = length * 0.8
    end if

    textArr = text.tokenize(newlineChar + newlineChar)

    i = 0
    while i < textArr.count() or i = 0
        while len(textArr[i]) < length * 0.5 and valid(textArr[i+1])
            if len(textArr[i] + newlineChar + textArr[i+1]) < length then
                textArr[i] = textArr[i] + newlineChar + textArr[i+1]
                textArr.delete(i+1)
            else
                exit while
            end if
        end while

        if len(textArr[i]) > length then
            if instr(length, textArr[i], " ") > 0 then
                pageLength = instr(length, textArr[i], " ")
                textArr = insertInArray(textArr, textArr[i].right(len(textArr[i]) - pageLength), i+1)
                textArr[i] = textArr[i].left(pageLength) + "..."
            end if
        else if len(textArr[i]) < 100 and valid(textArr[i+1]) then
            textArr[i] = textArr[i] + newlineChar + textArr[i+1]
            textArr.delete(i+1)
            i = i - 1
        end if

        i = i + 1
    end while

    return textArr
end function

function generateGuid() As String
    ' Ex. 5EF8541EC9F7CFCD4BD4036AF6C145DA
    return GetRandomHexString(33)
end function

function generateUUID(type4 = false As Boolean) As String
	uuid = ""
	for i=1 to 32
	        o = RND(16)
	        if o <= 10
	                o = o + 47
	        else
	                o = o + 96 - 10
	        end if
	        uuid = uuid + CHR(o)
	end for
	return uuid
end function

function getRandomHexString(length As Integer) As String
    hexChars = "0123456789ABCDEF"
    hexString = ""
    date = CreateObject("roDateTime").asSeconds()
    date = int(date / 16)
    for i = 1 to length-1
        rand = ((date + Rnd(16)) MOD 16) OR 0
        hexString = hexString + hexChars.Mid(rand, 1)
    end for
    return hexString
end function


function getDeviceID() As String 
    
    di = CreateObject("roDeviceInfo")
    return di.GetDeviceUniqueId()
    
end function
